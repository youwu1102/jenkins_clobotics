# -*- encoding:UTF-8 -*-
import optparse
from types import FunctionType
import logging

logger = logging.getLogger(__name__)
__parse = optparse.OptionParser(conflict_handler="resolve")
__parse.add_option('-j', '--job', dest='job', help=u'')
__parse.add_option('-s', '--stage', dest='stage', help=u'')


def execute_from_command_line(args=None):
    options, args = __parse.parse_args(args=args)
    if not options.job or not options.stage:
        exit('ERROR!must contain \"Job\" and \"Stage\" parameters!')

    import Jobs
    __print_attr(_class=Jobs)
    if options.job in dir(Jobs):
        jenkins_job = getattr(Jobs, options.job)
        __print_attr(_class=jenkins_job)
        if options.stage in dir(jenkins_job):
            stage = getattr(jenkins_job, options.stage)
            if isinstance(stage, FunctionType):
                return stage(args)
            else:
                raise AssertionError(u"stage \"{0}\" is not a function".format(options.stage))
        else:
            raise AttributeError(
                u"Stage \"{0}\" not in Jenkins Job [{1}].".format(options.stage, options.job))
    else:
        raise AttributeError(u"Job \"{0}\" is not existed.".format(options.job))


def __print_attr(_class):
    name = _class.__name__
    gap = " " * (len(name) - 3)
    logger.info(name)
    for _ in dir(_class):
        if not _.startswith('__'):
            logger.info(u"%s|-- %s" % (gap, _))
