# -*- encoding:UTF-8 -*-
import subprocess
import logging
import os

__logger = logging.getLogger(__name__)
environ = os.environ


def mkdir(path, *paths):
    path = os.path.join(path, *paths)
    if os.path.exists(path) and os.path.isdir(path):
        __logger.error('\"%s\" Folder already exists.' % path)
        return path
    else:
        if not os.path.exists(path):
            os.mkdir(path)
            return path
        else:
            __logger.error('ERROR: Unable to create folder \"%s\"' % path)
            return None


def getenv(attr_name):
    value = environ.get(key=attr_name, failobj=None)
    __logger.info("Try to get environment variable [ {key} ] and the value is [ {value} ]".format(key=attr_name, value=value))
    return value


class ExecuteResult(object):
    def __init__(self, exit_code, outputs):
        self._exit_code = exit_code
        self._outputs = outputs

    @property
    def exit_code(self):
        return self._exit_code

    @property
    def outputs(self):
        return self._outputs


def execute_command(command, encoding=None):
    outputs = list()
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE, shell=True)
    __logger.info('********************************************************')
    __logger.info('* EXECUTED COMMAND:\"%s\"' % command)
    try:
        for line in iter(p.stdout.readline, b''):
            if encoding is None:
                line = line.strip('\r\n')
            else:
                line = line.decode(encoding=encoding, errors="strict").strip('\r\n')
            __logger.info("* STDOUT: {line}".format(line=line))
            outputs.append(line)
    finally:
        exit_code = p.wait()
        p.kill()
        __logger.info('* EXIT CODE: \"%s\"' % exit_code)
        __logger.info('********************************************************')
        return ExecuteResult(exit_code=exit_code, outputs=outputs)


def execute_command_via_socket():
    pass


def execute_command_via_ssh():
    pass
