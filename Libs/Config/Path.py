# -*- encoding:UTF-8 -*-
import os
import sys

__ABS = os.path.abspath(os.path.dirname(sys.argv[0]))


def __get_host_name():
    import socket
    return socket.gethostname()


def __get_host_env():
    pass


def join(*args):
    p = os.path.join(*args)
    if not os.path.exists(p):
        os.makedirs(p)
    return p


def get_console_log_folder():
    host_name = __get_host_name()
    if host_name in ["DESKTOP-QIP584F"]:
        return "C:\Users\You\Desktop\Log"
    return join(__ABS, "Log")


def get_workspace_folder():
    host_name = __get_host_name()
    if host_name in ["DESKTOP-QIP584F"]:
        return "C:\Users\You\Desktop\Workspace"
    return __ABS


class Console(object):
    LOG_PATH = get_console_log_folder()


class Wind(object):
    WORKSPACE = join(get_workspace_folder(), "Wind")
