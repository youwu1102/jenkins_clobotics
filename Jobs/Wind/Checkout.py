# -*- encoding:UTF-8 -*-
from Libs import GeneralFunction as GF
from Libs.Config import Path


def main(*args, **kwargs):
    module = GF.getenv("MODULE")
    module = "DataCenter"
    if module in ["DataCenter", "AutoData", "ERP"]:
        module = "AutoDataProcess"
    workspace = GF.mkdir(Path.Wind.WORKSPACE, module)
    __init_git_repo(workspace=workspace)


def __init_git_repo(workspace):
    if workspace is None:
        raise EnvironmentError()
    




if __name__ == '__main__':
    main()
