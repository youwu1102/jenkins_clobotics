# -*- encoding:UTF-8 -*-
import sys
import Libs.Logger

if __name__ == "__main__":
    from Libs.OptionParse import execute_from_command_line

    execute_from_command_line(sys.argv[1:])
